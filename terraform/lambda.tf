data "archive_file" "empty_zip_code_lambda" {
  type        = "zip"
  output_path = "empty_lambda_code.zip"
  source {
    content  = "hello_world"
    filename = "dummy.txt"
  }
}

# resource aws_lambda_function s3_to_sqs_lambda
resource "aws_lambda_function" "s3_to_sqs_lambda" {
  # If the file is not in the current working directory you will need to include a
  # path.module in the filename.
  filename      = data.archive_file.empty_zip_code_lambda.output_path
  function_name = var.s3_to_sqs_lambda_name
  role          = aws_iam_role.iam_for_s3_to_sqs_lambda.arn
  handler       = "index.handler"
  memory_size = 512
  timeout = 900

  #source_code_hash = data.archive_file.lambda.output_base64sha256

  runtime = "nodejs18.x"

  environment {
    variables = {
      QUEUE_URL = aws_sqs_queue.q.url
    }
  }

}

# resource aws_lambda_permission allow_bucket
resource "aws_lambda_permission" "allow_bucket" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.s3_to_sqs_lambda.function_name
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.user_bucket_name.arn
}

# resource aws_lambda_function sqs_to_dynamo_lambda
resource "aws_lambda_function" "sqs_to_dynamo_lambda" {
  # If the file is not in the current working directory you will need to include a
  # path.module in the filename.
  filename      = data.archive_file.empty_zip_code_lambda.output_path
  function_name = var.sqs_to_dynamo_lambda_name
  role          = aws_iam_role.iam_for_sqs_to_dynamo_lambda.arn
  handler       = "index.handler"
  memory_size = 512
  timeout = 30

  #source_code_hash = data.archive_file.lambda.output_base64sha256

  runtime = "nodejs18.x"

  environment {
    variables = {
      TABLE_NAME = aws_dynamodb_table.job-table.name
    }
  }
}

# resource aws_lambda_permission allow_sqs_queue
resource "aws_lambda_permission" "allow_sqs_queue" {
  statement_id  = "AllowExecutionFromSQS"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.sqs_to_dynamo_lambda.function_name
  principal     = "sqs.amazonaws.com"
  source_arn    = aws_sqs_queue.q.arn
}

# resource aws_lambda_function job_api_lambda
resource "aws_lambda_function" "job_api_lambda" {
  # If the file is not in the current working directory you will need to include a
  # path.module in the filename.
  filename      = data.archive_file.empty_zip_code_lambda.output_path
  function_name = var.job_api_lambda_name
  role          = aws_iam_role.iam_for_job_api_lambda.arn
  handler       = "lambda.handler"
  memory_size = 512
  timeout = 30

  #source_code_hash = data.archive_file.lambda.output_base64sha256

  runtime = "nodejs18.x"

  environment {
    variables = {
      TABLE_NAME = aws_dynamodb_table.job-table.name
    }
  }
}


# resource aws_lambda_permission allow_api_gw

