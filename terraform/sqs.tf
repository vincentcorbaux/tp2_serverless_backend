# resource aws_sqs_queue job_offers_queue

resource "aws_sqs_queue" "q" {
  name = var.sqs_job_queue_name
}

resource "aws_sqs_queue_redrive_policy" "q" {
  queue_url = aws_sqs_queue.q.id
  redrive_policy = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.job_offers_queue_deadletter.arn
    maxReceiveCount     = 4
  })
}

# resource aws_sqs_queue job_offers_queue_deadletter
resource "aws_sqs_queue" "job_offers_queue_deadletter" {
  name = var.sqs_job_dead_letter_queue_name
  redrive_allow_policy = jsonencode({
    redrivePermission = "byQueue",
    sourceQueueArns   = [aws_sqs_queue.q.arn]
  })
}

# resource aws_lambda_event_source_mapping sqs_lambda_trigger

resource "aws_lambda_event_source_mapping" "sqs_lambda_trigger" {
  event_source_arn = aws_sqs_queue.q.arn
  function_name    = aws_lambda_function.sqs_to_dynamo_lambda.arn
}

