# resource aws_iam_role iam_for_s3_to_sqs_lambda
resource "aws_iam_role" "iam_for_s3_to_sqs_lambda" {
  name = var.s3_to_sqs_lambda_role_name

  assume_role_policy = file("files/lambda_assume_role_policy.json")
  tags = {
    tag-key = "tag-value"
  }
}

resource "aws_iam_role_policy" "policy_s3_bucket" {
  name = "policy_s3_bucket"
  role = aws_iam_role.iam_for_s3_to_sqs_lambda.id

  policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": ["s3:*","s3-object-lambda:*"]
        "Resource": "${aws_s3_bucket.user_bucket_name.arn}/*"
      }
    ]
  }
  )
}

resource "aws_iam_role_policy" "policy_sqs_queue" {
  name = "policy_sqs_queue"
  role = aws_iam_role.iam_for_s3_to_sqs_lambda.id

  policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": "sqs:SendMessage"
        "Resource":"${aws_sqs_queue.q.arn}"
      }
    ]
  }

  )
}

# resource aws_iam_role_policy lambda_logging_for_s3_to_sqs


#resource "aws_iam_role_policy" "lambda_logging_for_s3_to_sqs" {
#  name   = "lambda_logging_for_s3_to_sqs"
#  role   = aws_iam_role.iam_for_s3_to_sqs_lambda.id
#  policy = file("files/log_policy.json")
#}

# resource aws_iam_role_policy iam_policy_for_sqs_sender


# resource aws_iam_role_policy iam_policy_for_s3


# resource aws_iam_role iam_for_sqs_to_dynamo_lambda
resource "aws_iam_role" "iam_for_sqs_to_dynamo_lambda" {
  name = var.sqs_to_dynamo_lambda_role_name

  assume_role_policy = file("files/lambda_assume_role_policy.json")
  tags = {
    tag-key = "tag-value"
  }
}

resource "aws_iam_role_policy" "dynamo_to_sqs_role_policy" {
  name = "dynamo_to_sqs_role_policy"
  role = aws_iam_role.iam_for_sqs_to_dynamo_lambda.id

  policy = jsonencode({

    "Statement": [
      {
        "Effect": "Allow",
        "Action": ["sqs:ReceiveMessage",
                    "sqs:DeleteMessage",
                    "sqs:GetQueueAttributes"]
        "Resource": "${aws_sqs_queue.q.arn}/*"
      }
    ]
  }
  )
}

resource "aws_iam_role_policy" "dynamo_put_item_policy" {
  name = "dynamo_put_item_policy"
  role = aws_iam_role.iam_for_sqs_to_dynamo_lambda.id

  policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": "dynamodb:PutItem",
        "Resource":"${aws_dynamodb_table.job-table.arn}"
      }
    ]
  }

  )
}

# resource aws_iam_role_policy lambda_logging_for_sqs_to_dynamo
#resource "aws_iam_role_policy" "lambda_logging_for_sqs_to_dynamo" {
#  name   = "lambda_logging_for_sqs_to_dynamo"
#  role   = aws_iam_role.iam_for_sqs_to_dynamo_lambda.id
#  policy = file("files/log_policy.json")
#}

# resource aws_iam_role_policy iam_policy_for_dynamodb


# resource aws_iam_role_policy iam_policy_for_sqs_receiver


# resource aws_iam_role iam_for_job_api_lambda
resource "aws_iam_role" "iam_for_job_api_lambda" {
  name = var.job_api_lambda_role_name

  assume_role_policy = file("files/lambda_assume_role_policy.json")
  tags = {
    tag-key = "tag-value"
  }
}

resource "aws_iam_role_policy" "job_api_lambda_policy" {
  name = "job_api_lambda_policy"
  role = aws_iam_role.iam_for_job_api_lambda.id

  policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": "dynamodb:*",
        "Resource":"${aws_dynamodb_table.job-table.arn}"
      }
    ]
  }
  )
}

# resource aws_iam_role_policy iam_policy_for_dynamodb


# resource aws_iam_role_policy lambda_logging_for_job_api
#resource "aws_iam_role_policy" "lambda_logging_for_job_api" {
#  name   = "lambda_logging_for_job_api"
#  role   = aws_iam_role.iam_for_job_api_lambda.id
#  policy = file("files/log_policy.json")
#}

# resource aws_iam_role_policy_attachment lambda_exec_policy_attach
resource "aws_iam_role_policy_attachment" "lambda_exec_policy_attach" {
  role       = aws_iam_role.iam_for_job_api_lambda.name
  policy_arn = var.lambda_exec_policy_arn
}
